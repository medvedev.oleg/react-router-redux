import { FC } from "react";
 
export const Page404: FC = () => {
  return <>
    <h1>404</h1>
    <p>Страница не найдена</p>
  </>;
};
