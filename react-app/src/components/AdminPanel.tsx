import { FC, useState } from "react";
import { Badge, Button, Card, Form } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { homePageSlice } from "../store/slices/HomePage";


export const AdminPanel: FC = () => {
  const [validated, setValidated] = useState(false);
  const [isFeedAddedSuccess, setIsFeedAddedSuccess] = useState(false);

  const dispatch = useDispatch();

  const handlerOnSubmit = (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    setValidated(true);
    setIsFeedAddedSuccess(false);

    const form = event.currentTarget;

    if (form.checkValidity() === true) {
      dispatch(homePageSlice.actions.addNewsItem({
        id: Math.floor(Math.random() * 9999),
        group: form.formGroup.value,
        url: form.formUrl.value,
        text: form.formPreambule.value
      }))
      form.reset();
      setValidated(false);
      setIsFeedAddedSuccess(true);
    }

  }

  return (
    <div className="admin-panel">
      <h4>Добавить новость</h4>
      {isFeedAddedSuccess && <div style={{ textAlign: "center" }}><Badge bg="success">Новость успешно добавлена</Badge></div>}
      <Card style={{ padding: '20px', marginTop: "20px" }}>
        <Form noValidate onSubmit={handlerOnSubmit} validated={validated}>
          <Form.Group className="mb-3" controlId="formGroup">
            <Form.Label>Группа новостей</Form.Label>
            <Form.Control type="text" readOnly value="Новости со всего света" disabled />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formUrl">
            <Form.Label>Url на полное описание</Form.Label>
            <Form.Control type="text" required />
            <Form.Control.Feedback type="invalid">
              Поле не может быть пустым
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formPreambule">
            <Form.Label>Краткое описание</Form.Label>
            <Form.Control type="text" required />
            <Form.Control.Feedback type="invalid">
              Поле не может быть пустым
            </Form.Control.Feedback>

          </Form.Group>

          <Button variant="success" type="submit">Добавить</Button>
        </Form>
      </Card>
    </div>
  );
};
