import { FC } from "react";
import { Link } from "react-router-dom";
import { RouteNames } from "../router/router";


export const MainMenu: FC = () => {
  return (
    <div className="main-menu">
      <Link to={RouteNames.HOME_PAGE}>Основная страница</Link>
      <Link to={RouteNames.LOGIN}>Авторизация</Link>
      <Link to={RouteNames.REGISTER}>Регистрация</Link>      
    </div>
  );
};
