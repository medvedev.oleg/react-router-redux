import { createSlice } from "@reduxjs/toolkit"

export type HomePageNewsItem = {
    id: number,
    group: string,
    text: string,
    url: string
}

export type HomePageState = {
    news: HomePageNewsItem[]
}

const initialState: HomePageState = {
    news: [
        {
            id: 1,
            group: "Новости со всего света",
            text: "Французские фермеры анонсировали «полную блокаду» Парижа",
            url: "https://www.rbc.ru/society/27/01/2024/65b4e6659a7947616c7e2828?from=from_main_3"
        },
        {
            id: 2,
            group: "Новости со всего света",
            text: "Карты «Мир» с конца февраля заработают в Мьянме",
            url: "https://www.rbc.ru/economics/27/01/2024/65b4f98e9a7947ef50b467e0?from=copy"
        }
    ]
};

export const homePageSlice = createSlice({
    name: 'HomePage',
    initialState,
    reducers: {
        addNewsItem: (state, action: { payload: HomePageNewsItem }) => {
            state.news = [...state.news, action.payload]
        },
    }
})